<?php
	ini_set('session.cookie_httponly', 1);
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Fast & Furious</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="UTF-8">
</head>
<body>
	<?php include "topbar.html";

	if (!isset($_GET['lang']) && !isset($_SESSION['lang'])) {
		echo '<div class="welcome">
		<div class="lang-chooser">Please choose your language</div>
			<div class="languages">
				<a href="index.php?lang=en"><div class="lang-link">English</div></a>
				<a href="index.php?lang=ar"><div class="lang-link">العَرَبِيَّة</div></a>
				<a href="index.php?lang=ru"><div class="lang-link">Русский</div></a>
				<a href="index.php?lang=fa"><div class="lang-link">فارسی</div></a>
				<a href="index.php?lang=ku"><div class="lang-link">Kurdî</div></a>
			</div>	
		</div>';
	} else {
		//localization
		$_SESSION['lang'] = $_GET['lang'];
		include "lang/" . $_SESSION['lang'] . ".php";

		echo '<div class="warning">' . $_SESSION["strings"]["warning"] . '</div>';
		echo '<a href="questions.php?page=personalinfo"><div class="button-start">' . $_SESSION['strings']['start'] . '</div></a>';
	}

	?>
</body>
</html>