<?php
	$_SESSION['strings'] = array(
		'title1' => 'Agahdariya Kesane',
		'title2' => 'Rewşa tenduristiyê',
		'title3' => 'Rewşa zewacê',
		'title4' => 'Tenduristiya Giyanî',
		'title5' => 'Destdirêjiya zayendî',
		'title6' => 'Spas dikim',
		'warning' => 'Agahdariya rastîn dê pêvajoya serîlêdanê zûtir bike.',
		'warning-end' => 'Heke hûn daneyên çewt peyda bikin dibe ku hûn bi dozek sûc re rû bi rû bimînin û rewşa penaber winda bikin',
		'start' => 'Destpêkirin',
		'finish' => 'Qedandin',
		'next' => 'Piştî',
		'q1' => 'Ji kerema xwe navê xwe diyar bikin:',
		'q2' => 'Welatê ofiqlê ye?',
		'q3' => 'Welatê te ji ku hatî?',
		'q4' => 'Ji kerema xwe roja jidayikbûna xwe diyar bikin:',
		'q5' => 'Ji kerema xwe navên dêûbavên xwe binivîsin:',
		'q6' => 'Ji kerema xwe zayenda xwe hilbijêrin:',
		'q8' => 'Ji kerema xwe baweriyên xwe yên olî ragihînin:',
		'q9' => 'Asta perwerdehiya we çi ye?',
		'q10' => 'Ma ji we re nasnameyek heye?',
		'q11' => 'Pirsgirêkek bijîşkî ya ku hewcedariya tavilê hewce dike?',
		'q12' => 'Ger wusa be, ji kerema xwe rewşa xwe binivîse:',
		'q13' => 'Hûn zewicî ne / bi jinekê re?',
		'q14' => 'Ma ji we re arîkariya psîkolojîk hewce dike?',
		'q15' => 'Heke wusa be, ji kerema xwe hûrguliyan peyda bikin',
		'q16' => 'Ma we cinsî cinsî kiriye?',
		
	);