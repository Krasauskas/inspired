<?php
	// if (!isset($_GET['page'])) {
	// 	header("Location: index.php");
	// }

	// if (isset($_POST['pageSubmitted'])) {
	// 	foreach ($_POST as $key => $value) {
	// 		echo $key . " = " . $value . "</br>";
	// 	}
	// }
	ini_set('session.cookie_httponly', 1);
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Fast & Furious</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="UTF-8">
</head>
<body>
	<?php
		include "topbar.html";
		$page = "pages/" . $_GET['page'] . ".html";
		include $page;
	?>
</body>
</html>